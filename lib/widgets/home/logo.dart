import 'package:flutter/material.dart';
import 'package:fstore/models/app.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../widgets/home/search/custom_search.dart';
import 'search/custom_search_page.dart' as search;

class Logo extends StatelessWidget {
  final config;

  Logo({this.config, Key key}) : super(key: key);

  Widget renderLogo() {
    if (config['image'] != null) {
      if (config['image'].indexOf('http') != -1) {
        return Image.network(
          config['image'],
          height: 50,
        );
      }
      return Image.asset(
        config['image'],
        height: 40,
      );
    }
    return Image.asset(kLogoImage, height: 40);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Builder(
      builder: (context) {
        return Container(
          width: screenSize.width,
          color: config['background'] != null
              ? HexColor(config['background'])
              : Colors.transparent,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Container(
              width: screenSize.width /
                  (2 / (screenSize.height / screenSize.width)),
              constraints: BoxConstraints(
                minHeight: 50.0,
              ),
              child: Stack(
                textDirection: TextDirection.rtl,
                children: <Widget>[
                  if (config['showSearch'] ?? false)
                    Positioned(
                      // top: 55,
                      right: 10,
                      child: IconButton(
                        icon: Icon(
                          Provider.of<AppModel>(context, listen: false)
                                      .locale ==
                                  'ar'
                              ? Icons.blur_on
                              : Icons.search,
                          color: config['color'] != null
                              ? HexColor(config['color'])
                              : Theme.of(context).accentColor.withOpacity(0.6),
                          size: 22,
                        ),
                        onPressed: () {
                          Provider.of<AppModel>(context, listen: false)
                                      .locale ==
                                  'ar'
                              ? openDrawer(context)
                              : search.showSearch(
                                  context: context, delegate: CustomSearch());
                          ;
                        },
                      ),
                    ),
                  if (config['showMenu'] ?? false)
                    Positioned(
                      // top: 55,
                      left: 10,
                      child: IconButton(
                        icon: Icon(
                          Provider.of<AppModel>(context, listen: false)
                                      .locale ==
                                  'ar'
                              ? Icons.search
                              : Icons.blur_on,
                          color: config['color'] != null
                              ? HexColor(config['color'])
                              : Theme.of(context).accentColor.withOpacity(0.9),
                          size: 22,
                        ),
                        onPressed: () {
                          Provider.of<AppModel>(context, listen: false)
                                      .locale ==
                                  'ar'
                              ? search.showSearch(
                                  context: context, delegate: CustomSearch())
                              : openDrawer(context);
                          ;
                        },
                      ),
                    ),
                  Container(
                    constraints: BoxConstraints(minHeight: 50),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          if (config['showLogo'] ?? false)
                            Center(child: renderLogo()),
                          if ((config['showLogo'] ?? true) &&
                              config['name'] != null)
                            SizedBox(
                              width: 5,
                            ),
                          if (config['name'] != null)
                            Text(
                              config['name'],
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

openDrawer(context) {
  eventBus.fire('drawer');
  Scaffold.of(context).openDrawer();
}
