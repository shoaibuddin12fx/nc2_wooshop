import 'package:flutter/material.dart';

import 'address.dart';
import 'cart/cart_base.dart';
import 'payment_settings.dart';

import '../services/index.dart';

class CreditCardModel extends ChangeNotifier {
  final Services _service = Services();
  CreditCard creditCard;
  bool isLoading = true;
  String message;

  void setCreditCard(String cardNumber, String cardHolderName, String expiryDate, String cvv) {
    creditCard.setCreditCard(cardNumber, cardHolderName, expiryDate, cvv);
  }

  // ignore: missing_return
  Future<String> checkoutWithCreditCard(String vaultId, CartModel cartModel, Address address,
      PaymentSettingsModel paymentSettingsModel) async {
    try {
      var result = await _service.checkoutWithCreditCard(vaultId, cartModel, address, paymentSettingsModel);

      isLoading = false;
      message = null;
      notifyListeners();

      return result;
    } catch (err) {
      isLoading = false;
      message = "There is an issue with the app during request the data, please contact admin for fixing the issues " +
          err.toString();
      notifyListeners();
    }
  }

  CreditCard getCreditCard() {
    return creditCard;
  }
}

class CreditCard {
  String cardNumber;
  String cardHolderName;
  String expiryDate;
  String cvv;

  CreditCard({this.cardNumber, this.cardHolderName, this.expiryDate, this.cvv});

  void setCreditCard(String cardNumber, String cardHolderName, String expiryDate, String cvv) {
    this.cardNumber = cardNumber;
    this.cardHolderName = cardHolderName;
    this.expiryDate = expiryDate;
    this.cvv = cvv;
  }

  Map<String, dynamic> toJson() {
    return {
      "cardNumber": cardNumber,
      "cardHolderName": cardHolderName,
      "expiryDate": expiryDate,
      "cvv": cvv,
    };
  }

  CreditCard.fromLocalJson(Map<String, dynamic> json) {
    try {
      cardNumber = json['cardNumber'];
      cardHolderName = json['cardHolderName'];
      expiryDate = json['expiryDate'];
      cvv = json['cvv'];
    } catch (e) {
      print(e.toString());
    }
  }
}
